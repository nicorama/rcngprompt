/*
 * rcNgPrompt - very simple angular replacement of window.prompt
 * http://github.com/jean-rakotozafy/rcNgPrompt
 * (c) 2014 MIT License, https://www.m-ite.com
 */

(function (window, angular) {
    'use strict';

    var module = angular.module('rcNgPrompt', []);
    var $el = angular.element;

    module.factory('rcNgPrompt', ['$document', '$compile', '$q', '$rootScope', '$timeout', '$window',
        function ($document, $compile, $q, $rootScope, $timeout, $window) {

            var defaults = this.defaults = {
                width: 400,
                showClose: true,
                closeByDocument: false,
                closeByEscape: false,
                data: {value: ''},
                modal: true,
                inline: false,
                ctrlName: 'promptCtrl'
            };

            var global = {};

            var $body = $document.find('body');

            var privateMethods = {
                closeDialog: function ($prompt, $blackout) {
                    if ($prompt) {
                        $prompt.unbind('click');
                        $prompt.scope().$destroy();
                        $prompt.remove();
                    }
                    if ($blackout) {
                        $blackout.remove();
                    }
                    if (global.defer) {
                        delete global.defer;
                    }
                },

                centerBox: function ($prompt, height, width) {
                    $prompt.css({
                        position: 'fixed',
                        top: '50%',
                        left: '50%',
                        width: width + 'px',
                        height: height + 'px',
                        marginLeft: (-width / 2) + 'px',
                        marginTop: (-height / 2) + 'px'
                    });
                },

                showBlackout: function ($body) {

                    var $blackout = $el('<div/>').attr('id', 'blackout').addClass('blackout');
                    $body.append($blackout);
                    var d = $document[0],
                        e = $document[0].documentElement,
                        g = d.getElementsByTagName('body')[0],
                        ww = $window.innerWidth || e.clientWidth || g.clientWidth,
                        wh = $window.innerHeight || e.clientHeight || g.clientHeight;


                    $blackout.css({
                        display: 'block',
                        width: ww + 'px',
                        height: wh + 'px'
                    });
                    return $blackout;
                }

            };

            var proxy = {
                open: function (opts) {
                    //var self = this;
                    var options = angular.copy(defaults);

                    opts = opts || {};
                    angular.extend(options, opts);

                    var title = options.title || 'Prompt';
                    //var defer = $q.defer();

                    var scope = angular.isObject(options.scope) ? options.scope.$new() : $rootScope.$new();
                    scope.result = {};
                    var $prompt = $el('<div/>').attr('id', 'popup-box').addClass('popup-box');

                    var $close = $el('<div/>').addClass('close').text('X');
                    $prompt.append($close);
                    $prompt.append('<div class="top"><h2>' + title + '</h2></div>');

                    var $content = $el('<div/>').attr('id', 'content').addClass('bottom');
                    $prompt.append($content);

                    var $blackout;
                    if (options.modal) {
                        $blackout = privateMethods.showBlackout($body);
                    }

                    if (options.controller && angular.isString(options.controller)) {
                        $prompt.attr('ng-controller', options.controller);
                    }

                    if (options.data) {

                        if (angular.isString(options.data)) {
                            scope.data = options.data.replace(/^\s*/, '')[0] === '{' ? angular.fromJson(options.data) : options.data;
                        } else if (angular.isObject(options.data)) {
                            scope.data = options.data;
                        }

                        angular.forEach(scope.data, function (value, key) {
                            if (angular.isString(value) || typeof value === 'boolean') {
                                this[key] = value;
                            }
                        }, scope.result);

                        var $fields = $el('<div class="ng-prompt-fields"/>');
                        for (var key in scope.data) {
                            if (key.substr(0, 1) === '$') {
                                continue;
                            }
                            if (typeof scope.data[key] === 'boolean') {
                                var $checkbox = $el('<label><span>' + key + '</span><input ng-checked="data.' + key + '" type="checkbox" ng-model="result.' + key + '"  /></label>');
                                $fields.append($checkbox);
                            } else if (angular.isArray(scope.data[key])) {
                                var $select = $el('<label><span>' + key + '</span><select ng-model="result.' + key + '" ng-options="d for d in data.' + key + '" /></label>');
                                $fields.append($select);
                            } else {
                                var $input = $el('<label><span>' + key + '</span><input type="text" ng-model="result.' + key + '" /></label>');
                                $fields.append($input);
                            }
                        }
                    }

                    var $okBtn = $el('<button class="ng-prompt-ok" id="ngPromptOk">Submit</button>');
                    var $cancelBtn = $el('<button class="ng-prompt-cancel" id="ngPromptCancel">Cancel</button>');

                    $timeout(function () {
                        $prompt.css('display', 'block');
                        $content.append($fields).append($okBtn).append($cancelBtn);

                        $compile($prompt)(scope);
                        $body.append($prompt);

                        var h = options.height || $prompt[0].clientHeight;
                        privateMethods.centerBox($prompt, h, options.width);
                    });

                    global.defer = $q.defer();

                    $close.on('click', function (e) {
                        global.defer.reject();
                        privateMethods.closeDialog($prompt, $blackout);
                    });

                    $okBtn.on('click', function (e) {
                        global.defer.resolve(scope.result);
                        privateMethods.closeDialog($prompt, $blackout);
                    });

                    $cancelBtn.on('click', function (e) {
                        global.defer.reject();
                        privateMethods.closeDialog($prompt, $blackout);
                    });

                    return{
                        id: $prompt.attr('id'),
                        promise: global.defer.promise,
                        close: function () {
                            privateMethods.closeDialog($prompt, $blackout);
                        }
                    };
                }
            };

            return proxy;

        }]);

})(window, window.angular);
